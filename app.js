require("dotenv").config();
const express = require("express");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const router = require("./src/routes");
const path = require("path");
const app = express();
const dir = path.join(__dirname, 'public');

app.use(bodyParser.json({ limit: '100mb', extended: true }));
app.use(bodyParser.urlencoded({
    limit: "100mb",
    extended: true
}));
app.use(cookieParser());
app.use(cors('Access-Control-Allow-Origin: *'));
app.use('/api/v1', router);
app.use(express.static(dir))

module.exports = app;