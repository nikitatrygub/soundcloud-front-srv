require("dotenv").config();
const config = require("./config.js");
const app = require("./app");
const http = require("http");
const server = http.createServer(app);
const log = require("./config/logger");
const { connect } = require("./src/sockets/index");

connect(server);

server.listen(config.port, () => {
    log.info(`Server running on port ${config.port}`)
});


