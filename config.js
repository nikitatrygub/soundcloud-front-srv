require("dotenv").config();

module.exports = {
    logLevel: process.env.logLevel || 'debug',
    port: process.env.port || 3001,
    dbname: process.env.dbname,
    dbuser: process.env.dbuser,
    dbpassword: process.env.dbpassword,
    host: process.env.host
}