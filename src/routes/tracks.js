const express = require("express");
const router = express.Router();
const {
    createTrack,
    getUploadedImage,
    getUploadedAudio,
    getRelatedTracks,
    getOneTrack,
    getTracksByGenres,
    likeTrack,
    unlikeTrack,
    getLikedTracks
} = require("../controllers/tracks");

router.get("/related/:userId", getRelatedTracks);
router.get("/:trackId/user/:userId", getOneTrack);
router.get("/user/:id/favourites", getTracksByGenres);
router.get("/user/:userId/liked", getLikedTracks);

router.post("/create", createTrack);

router.get("/uploads/thumbs/:file", getUploadedImage);
router.get("/uploads/audio/:file", getUploadedAudio);

router.get("/like/:userId/:trackId", likeTrack);
router.get("/unlike/:userId/:trackId", unlikeTrack);

module.exports = router