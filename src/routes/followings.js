const express = require("express");
const router = express.Router();
const { follow, unfollow } = require("../controllers/followings");

router.post('/follow/:userId', follow);
router.post('/unfollow/:userId', unfollow);

module.exports = router;