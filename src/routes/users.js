const express = require("express");
const router = express.Router();
const { createUser, updateUser, loginUser, changePassword } = require("../controllers/users");

router.post("/create", createUser);
router.put("/update/:id", updateUser);
router.post('/login', loginUser);
router.post('/changePassword/:id', changePassword);

module.exports = router;