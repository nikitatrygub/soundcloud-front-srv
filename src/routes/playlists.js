const express = require("express");
const router = express.Router();
const { createPlaylist, getUserPlaylists, addTrackToPlaylist } = require("../controllers/playlists");

router.post('/create/:id', createPlaylist);
router.get('/user/:id', getUserPlaylists);
router.get('/add/:trackId/playlist/:playlistId', addTrackToPlaylist);

module.exports = router