const express = require('express');
const router = express.Router();
const { createSession } = require("../controllers/listeningSessions");

router.post('/createSession', createSession);

module.exports = router;