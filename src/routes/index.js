const express = require("express");
const router = express.Router();
const users = require("./users");
const tracks = require("./tracks");
const genres = require("./genres");
const playlists = require("./playlists");
const followings = require("./followings");
const listeningSessions = require("./listeningSessions");


router.use('/users', users)
router.use('/tracks', tracks);
router.use('/genres', genres);
router.use('/playlists', playlists);
router.use('/followings', followings);
router.use('/listeningSessions', listeningSessions);

module.exports = router;