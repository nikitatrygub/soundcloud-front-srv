module.exports = (sequalize, DataTypes) => {
    return sequalize.define(
        'listeningSession',
        {
            id: {
                type: DataTypes.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
                field: 'id'
            },
            userId: {
                type: DataTypes.INTEGER(11),
                allowNull: true,
                field: "user_id",
            },
            trackId: {
                type: DataTypes.INTEGER(11),
                allowNull: true,
                field: "track_id"
            },
            genres: {
                type: DataTypes.STRING(128),
                allowNull: true,
                field: "track_genres"
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "created_at"
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "updated_at"
            },
            deletedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "deleted_at"
            }
        },
        {
            tableName: 'listeningSession'
        }
    )
}
