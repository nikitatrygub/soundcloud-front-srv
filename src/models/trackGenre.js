module.exports = (sequalize, DataTypes) => {
    return sequalize.define(
        'track_genre',
        {
            id: {
                type: DataTypes.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
                field: 'id'
            },
            genreId: {
                type: DataTypes.INTEGER(11),
                allowNull: true,
                field: "genre_id"
            },
            trackId: {
                type: DataTypes.INTEGER(11),
                allowNull: true,
                field: "track_id",
                references: {
                    model: 'tracks',
                    key: 'id'
                }
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "created_at"
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "updated_at"
            },
            deletedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "deleted_at"
            }
        },
        {
            tableName: 'track_genre'
        }
    )
}
