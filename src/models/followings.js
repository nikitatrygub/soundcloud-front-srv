module.exports = (sequalize, DataTypes) => {
    return sequalize.define(
        'followings',
        {
            id: {
                type: DataTypes.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
                field: 'id'
            },
            subscriberId: {
                type: DataTypes.INTEGER(11),
                allowNull: true,
                field: "subscriber_id",
                references: {
                    model: 'users',
                    key: 'id'
                }
            },
            subscribeUserId: {
                type: DataTypes.INTEGER(11),
                allowNull: true,
                field: "subscribe_user_id",
                references: {
                    model: 'users',
                    key: 'id'
                }
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "created_at"
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "updated_at"
            },
            deletedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "deleted_at"
            }
        },
        {
            tableName: 'followings'
        }
    )
}
