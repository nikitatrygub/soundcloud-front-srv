module.exports = (sequalize, DataTypes) => {
    return sequalize.define(
        'users',
        {
            id: {
                type: DataTypes.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
                field: 'id'
            },
            email: {
                type: DataTypes.STRING(64),
                allowNull: false,
                field: 'email'
            },
            password: {
                type: DataTypes.STRING(64),
                allowNull: false,
                field: 'password'
            },
            name: {
                type: DataTypes.STRING(64),
                allowNull: false,
                field: 'name'
            },
            genres: {
                type: DataTypes.STRING(64),
                allowNull: true,
                field: 'genres'
            },
            photoUrl: {
                type: DataTypes.STRING(128),
                allowNull: true,
                field: 'photo_url'
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "created_at"
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "updated_at"
            },
            deletedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "deleted_at"
            }
        },
        {
            tableName: 'users'
        }
    )
}
