module.exports = (sequalize, DataTypes) => {
    return sequalize.define(
        'track_playlist',
        {
            id: {
                type: DataTypes.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
                field: 'id'
            },
            trackId: {
                type: DataTypes.INTEGER(11),
                allowNull: true,
                field: "track_id",
                references: {
                    model: 'tracks',
                    key: 'id'
                }
            },
            playlistId: {
                type: DataTypes.INTEGER(11),
                allowNull: true,
                field: "playlist_id"
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "created_at"
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "updated_at"
            },
            deletedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "deleted_at"
            }
        },
        {
            tableName: 'track_playlist'
        }
    )
}
