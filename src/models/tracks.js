module.exports = (sequalize, DataTypes) => {
    return sequalize.define(
        'tracks',
        {
            id: {
                type: DataTypes.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
                field: 'id'
            },
            userId: {
                type: DataTypes.INTEGER(11),
                allowNull: true,
                primaryKey: false,
                references: {
                    model: 'users',
                    key: 'id'
                },
                field: 'user_id'
            },
            title: {
                type: DataTypes.STRING(32),
                allowNull: true,
                field: "title"
            },
            artist: {
                type: DataTypes.STRING(128),
                allowNull: true,
                field: "artist"
            },
            description: {
                type: DataTypes.STRING(512),
                allowNull: true,
                field: "description"
            },
            audioUrl: {
                type: DataTypes.STRING(128),
                allowNull: true,
                field: "audio_url"
            },
            imgUrl: {
                type: DataTypes.STRING(128),
                allowNull: true,
                field: "img_url"
            },
            likes: {
                type: DataTypes.INTEGER(16),
                allowNull: true,
                defaultValue: 0,
                field: "likes"
            },
            duration: {
                type: DataTypes.INTEGER(128),
                allowNull: true,
                field: "duration"
            },
            countOfListen: {
                type: DataTypes.INTEGER(11),
                allowNull: true,
                field: "count_of_listen",
                defaultValue: 0
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "created_at"
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "updated_at"
            },
            deletedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "deleted_at"
            }
        },
        {
            tableName: 'tracks'
        }
    )
}
