module.exports = (sequalize, DataTypes) => {
    return sequalize.define(
        'playlists',
        {
            id: {
                type: DataTypes.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
                field: 'id'
            },
            userId: {
                type: DataTypes.INTEGER(11),
                allowNull: true,
                field: "user_id",
                references: {
                    model: 'users',
                    key: 'id'
                }
            },
            title: {
                type: DataTypes.STRING(128),
                allowNull: false,
                field: "title"
            },
            userName: {
                type: DataTypes.STRING(128),
                allowNull: true,
                field: "user_name"
            },
            isPrivate: {
                type: DataTypes.BOOLEAN,
                allowNull: true,
                defaultValue: false,
                field: "is_private"
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "created_at"
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "updated_at"
            },
            deletedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: "deleted_at"
            }
        },
        {
            tableName: 'playlists'
        }
    )
}
