const { tracks, users, trackGenre, trackLikes } = require("../../config/sequelize");
const Sequalize = require("sequelize");
const log = require("../../config/logger");
const fs = require("fs");
const Op = Sequalize.Op;
const musicMetadata = require("music-metadata");
const util = require('util')

const createTrack = async (fields) => {
    const { imgUrl, imgName, audioUrl, audioName, genreIds } = fields;



    let duration = null;
    if (imgUrl) await uploadTrackImage(imgUrl, imgName);
    if (audioUrl) {
        let uploadedImage = await uploadTrackAudio(audioUrl, audioName)

        if (!uploadedImage) return errorHandler(500, e.message);
        else duration = uploadedImage
    }

    const generateUrlForImageUploads = (imgName) => {
        if (imgName) {
            return `/uploads/thumbs/${imgName}`
        }
        return null
    }
    const generateUrlForAudioUploads = (audioName) => {
        if (audioName) {
            return `/uploads/audio/${audioName}`
        }
        return null
    }
    const dataForInsert = {
        ...fields,
        duration,
        audioUrl: generateUrlForAudioUploads(audioName),
        imgUrl: generateUrlForImageUploads(imgName)
    }
    const track = await tracks.create(dataForInsert);
    const { id } = track;

    if (genreIds) {
        const trackGenreFields = genreIds.map((genre) => {
            return {
                trackId: id,
                genreId: genre
            }
        })
        await trackGenre.bulkCreate(trackGenreFields)
            .then(response => response)
            .catch(e => { return errorHandler(500, e.message) })
    }

    if (!track) {
        return errorHandler(500, e.message);
    }

    return track;
}

const uploadTrackImage = async (imgUrl, imgName) => {

    const getImgSrc = imgUrl.replace(/^data:image\/\w+;base64,/, "");
    const buffer = new Buffer.from(getImgSrc, "base64");

    const writeImage = await fs.writeFile(`uploads/thumbs/${imgName}`, buffer, function (err) {
        if (err) {
            return errorHandler(500, e.message)
        }
        return true
    })

    return writeImage
}

const uploadTrackAudio = async (audioUrl, audioName) => {
    const getAudioSrc = audioUrl.replace(/^data:audio\/\w+;base64,/, "");
    const buffer = new Buffer.from(getAudioSrc, "base64");

    return new Promise((resolve, reject) => {
        fs.writeFile(`uploads/audio/${audioName}`, buffer, (err) => {
            if (err) reject(err)
            else {
                musicMetadata.parseFile(`./uploads/audio/${audioName}`, { native: true })
                    .then(metadata => {
                        const duration = util.inspect(metadata.format.duration)
                        resolve(duration)
                        // reject(false)
                    })
                    .catch(error => {
                        log.trace("parse error", error)
                        reject(errorHandler(500, error.message))
                    });
            }
        })
    })
}

const getRelatedTracks = async (userId) => {
    const userLikes = await trackLikes.findAll({
        where: {
            userId: userId
        }
    })
        .then(response => response)
        .catch(e => { return errorHandler(500, e.message) })

    const currentDate = new Date();
    const tenDaysLeft = currentDate.setDate(currentDate.getDate() - 10);
    const allTracks = await tracks.findAll({
        where: {
            createdAt: {
                [Op.gt]: tenDaysLeft
            }
        }
    })
        .then(tracks => tracks)
        .catch(e => { errorHandler(500, e.message) });

    const mapAllracksToLikes = allTracks.map((track) => ({
        ...track.dataValues,
        isLiked: userLikes.some(like => like.trackId === track.id)
    }))


    return mapAllracksToLikes;
}

const getOneTrack = async (trackId, userId) => {
    const trackLike = await trackLikes.findOne({
        where: {
            trackId: trackId,
            userId: userId
        }
    })
        .then(response => response)
        .catch(e => { return errorHandler(500, e.message) });

    const track = await tracks.findByPk(trackId)
        .then(track => {
            if (track) {
                const updatedTrack = {
                    ...track.dataValues,
                    isLiked: trackLike ? true : false
                }
                return updatedTrack;
            }
            return null
        })
        .catch(e => { return errorHandler(500, e.message) });

    return track;
}

const getTracksByGenres = async (userId) => {
    const user = await users.findById(userId);
    const userGenres = JSON.parse(user.genres);
    if (userGenres) {
        const genreIds = userGenres.map((genreId) => genreId);
        const findTrackGenre = await trackGenre.findAll({
            where: {
                genreId: genreIds
            }
        })
            .then(response => response)
            .catch(e => { return errorHandler(500, e.message) })

        const trackIds = findTrackGenre.map((field) => field.trackId)

        function unique(arr) {
            var result = [];

            nextInput:
            for (var i = 0; i < arr.length; i++) {
                var str = arr[i]; // для каждого элемента
                for (var j = 0; j < result.length; j++) { // ищем, был ли он уже?
                    if (result[j] == str) continue nextInput; // если да, то следующий
                }
                result.push(str);
            }

            return result;
        }

        const uniqTrackIds = unique(trackIds);

        const findTracks = await tracks.findAll({
            where: {
                id: uniqTrackIds
            }
        })
            .then(response => response)
            .catch(e => { return errorHandler(500, e.message) })
        return findTracks;
    }

    return null

}

const getLikedTracks = async (userId) => {
    const likedTracks = await trackLikes.findAll({
        where: {
            userId: userId
        }
    })

    if (likedTracks.length > 0) {
        const trackIds = likedTracks.map((liked) => liked.trackId)

        const findedTracks = await tracks.findAll({
            where: {
                id: trackIds
            }
        })

        if (findedTracks) return findedTracks;

    }

    return null

}

const likeTrack = async (userId, trackId) => {
    const dataForInsertIntoTrackLikes = {
        userId,
        trackId
    }
    const [like, isLiked] = await trackLikes.findOrCreate({
        where: {
            userId: userId,
            trackId: trackId
        },
        default: { ...dataForInsertIntoTrackLikes }
    })
        .then(like => like)
        .catch(e => { return errorHandler(500, e.message) })

    if (isLiked) {
        const track = await tracks.findByPk(trackId);

        track.likes = track.likes + 1;
        await track.save();

        return track;
    }
    return null

}

const unlikeTrack = async (userId, trackId) => {
    const unlike = await trackLikes.destroy({
        where: {
            userId: userId,
            trackId: trackId
        }
    })
        .then(response => response)
        .catch(e => { return errorHandler(500, e.message) });


    if (unlike !== 0) {

        const track = await tracks.findByPk(trackId);
        track.likes = track.likes - 1;
        await track.save();
        return track;
    }

    return null

}

module.exports = { createTrack, getRelatedTracks, getOneTrack, getTracksByGenres, likeTrack, unlikeTrack, getLikedTracks };