const { followings } = require("../../config/sequelize");

const follow = async (subscriberId, subscribeUserId) => {
    const [follow, isFollowed] = await followings.findOrCreate({
        where: {
            subscriberId: subscriberId,
            subscribeUserId: subscribeUserId
        },
        defaults: { subscriberId, subscribeUserId }
    });

    if (!isFollowed) { return errorHandler(409, "You already followed") };

    return follow;
}

const unfollow = async (subscriberId, subscribeUserId) => {
    const following = await followings.find({
        where: {
            subscriberId: subscriberId,
            subscribeUserId: subscribeUserId
        }
    })
        .then(follow => {
            return follow.destroy();
        })
        .then(response => response)
        .catch(e => { return errorHandler(404, "Following not found") })

    return following;
}

module.exports = {
    follow,
    unfollow
}