const { genres } = require("../../config/sequelize");
const log = require("../../config/logger");

const getGenres = async () => {
    const allGenres = await genres.findAll()
        .then(genres => genres)
        .catch(e => { return errorHandler(500, e.message) });
    return allGenres;
}

module.exports = {
    getGenres
}