const { listeningSessions, tracks } = require("../../config/sequelize");
const log = require("../../config/logger");

const createSession = async (userId, trackId) => {
    await tracks.findByPk(trackId)
        .then(track => {
            track.countOfListen = track.countOfListen + 1;
            track.save()
        })
    const session = await listeningSessions.findOrCreate({
        where: {
            userId: userId,
            trackId: trackId
        }
    })
        .then(([session, created]) => {
            if (created) return session;
            throw Error("Session is exist");
        })
        .catch(error => { return errorHandler(500, error.message) });

    return session
}

module.exports = {
    createSession
}