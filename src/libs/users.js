const { users } = require("../../config/sequelize");
const { errorHandler } = require("./errorHandler");
const log = require("../../config/logger");
const fs = require("fs");

const createUser = async (fields) => {
    const { img, email, password, name, genres } = fields;
    const { blob, imgName } = img;

    const generateUrlForImage = () => {
        if (blob) {
            return `/uploads/profile/${imgName}`
        }
        return null
    }

    if (blob) await uploadUserPhoto(blob, imgName);

    const dataForInsert = {
        email,
        name,
        password,
        genres,
        photoUrl: generateUrlForImage()
    }

    const user = await users.findOrCreate({
        where: { email: fields.email },
        defaults: { ...dataForInsert }
    }).then(([user, created]) => {
        if (created) return user
        return errorHandler(403, "Incorrect password");
    }).catch(e => { return errorHandler(500, err.message) })

    return user;
}

const updateUser = async (userId, fields) => {
    const { beforePassword, email, name, genres, photoUrl } = fields;
    const user = await users.findByPk(userId)
        .then(user => {
            log.trace(user.password, beforePassword)
            if (user.password === beforePassword) {
                return user.update({ email, name, genres, photoUrl });
            }
            return errorHandler(403, "Incorrect password");;
        })
        .catch(e => { return errorHandler(500, err.message) })


    return user;
}

const loginUser = async (fields) => {
    const { email, password } = fields;
    const user = await users.find({
        where: {
            email: email
        }
    })

    if (user) {
        if (user.password === password) {
            return user
        }
        else {
            return errorHandler(403, "Incorrect password");
        }
    }

    return errorHandler(404, "User not found");
}

const changePassword = async (userId, newPassword, beforePassword) => {
    const findUserAndUpdate = await users.findByPk(userId)
        .then(user => {
            if (user.password === beforePassword) {
                user.password = newPassword
                return user.save();
            }
            return errorHandler(403, "Incorrect password");
        })
        .catch(e => { return errorHandler(500, e.message); });

    return findUserAndUpdate;
}

const uploadUserPhoto = async (blob, imgName) => {
    const getImgSrc = blob.replace(/^data:image\/\w+;base64,/, "");
    const buffer = new Buffer.from(getImgSrc, "base64");

    const image = fs.writeFile(`uploads/profile/${imgName}`, buffer, (err) => {
        if (err) return errorHandler(500, err.message);
    })

    return image;
}


module.exports = {
    createUser,
    updateUser,
    loginUser,
    changePassword
}