const { playlists, trackPlaylist } = require("../../config/sequelize");
const log = require("../../config/logger");

const createPlaylist = async (fields, userId) => {

    const dataForInsert = {
        ...fields,
        userId: userId
    }
    const playlist = await playlists.create(dataForInsert)
        .then(playlist => playlist)
        .catch(e => { return errorHandler(500, e.message) })

    return playlist
}

const getUserPlaylists = async (userId) => {
    const userPlaylists = await playlists.findAll({
        where: {
            userId: userId
        }
    })
        .then(userPlaylists => {
            if (userPlaylists.length > 0) return userPlaylists
            return null
        })
        .catch(e => { return errorHandler(500, e.message) });

    return userPlaylists;

}

const addTrackToPlaylist = async (trackId, playlistId) => {
    const [trackInPlaylist, isCreated] = await trackPlaylist.findOrCreate({
        where: {
            trackId: trackId,
            playlistId: playlistId
        },
        default: { trackId, playlistId }
    });

    if (!isCreated) return errorHandler(409, 'Track already in playlist');

    return trackInPlaylist;
}

module.exports = {
    createPlaylist,
    getUserPlaylists,
    addTrackToPlaylist
}
