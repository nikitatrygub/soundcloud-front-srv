const { createTrack, getRelatedTracks, getOneTrack, getTracksByGenres, likeTrack, unlikeTrack, getLikedTracks } = require("../libs/tracks");
const fs = require("fs");
const log = require("../../config/logger");

module.exports = {
    getRelatedTracks: async (req, res) => {
        try {
            const { userId } = req.params;
            const tracks = await getRelatedTracks(userId);

            res.status(200).json({
                tracks
            })
        } catch (e) {
            res.status(e.status || 500).json({
                status: "ERROR",
                message: e.message
            })
        }
    },
    getOneTrack: async (req, res) => {
        try {
            const { trackId, userId } = req.params;
            const track = await getOneTrack(trackId, userId);

            res.status(200).json({
                track
            })
        } catch (e) {
            res.status(e.status || 500).json({
                message: e.message
            })
        }
    },
    getTracksByGenres: async (req, res) => {
        try {
            const { id } = req.params;
            log.trace(id)
            const tracks = await getTracksByGenres(id);

            res.status(200).json({
                message: "OK",
                tracks
            })
        } catch (e) {
            res.status(e.status || 500).json({
                message: e.message
            })
        }
    },
    createTrack: async (req, res) => {
        try {
            const fields = req.body;
            const track = await createTrack(fields);

            return res.status(200).json({
                status: "OK",
                message: "Track is created",
                data: track
            })
        } catch (e) {
            log.error(e);
            return res.status(e.status || 500).json({
                status: "Error",
                message: "Track is't created",
                error: e.message
            })
        }
    },
    getUploadedImage: async (req, res) => {
        try {
            let img = fs.readFileSync(`./uploads/thumbs/${req.params.file}`);
            log.trace(img)
            res.status(200).end(img, 'binary');
        } catch (e) {
            res.status(e.status || 500).json({
                message: "Image not found"
            })
        }
    },
    getUploadedAudio: async (req, res) => {
        try {
            fs.exists(`./uploads/audio/${req.params.file}`, (exists) => {
                if (exists) {
                    let stream = fs.createReadStream(`./uploads/audio/${req.params.file}`);
                    stream.pipe(res);
                    return;
                }
                res.status(e.status || 500).json({
                    message: "File not found"
                })
            })
        } catch (e) {
            res.status(e.status || 500).json({
                message: e.message
            })
        }
    },
    getLikedTracks: async (req, res) => {
        try {
            const { userId } = req.params;
            const tracks = await getLikedTracks(userId);

            res.status(200).json({
                tracks
            })
        } catch (e) {
            res.status(e.status || 500).json({
                message: e.message
            })
        }
    },
    likeTrack: async (req, res) => {
        try {
            const { userId, trackId } = req.params;
            await likeTrack(userId, trackId);

            res.status(200).json({
                message: "OK"
            })
        } catch (e) {
            res.status(e.status || 500).json({
                message: e.message
            })
        }
    },
    unlikeTrack: async (req, res) => {
        try {
            const { userId, trackId } = req.params;
            await unlikeTrack(userId, trackId);

            res.status(200).json({
                message: "OK"
            })
        } catch (e) {
            res.status(e.status || 500).json({
                message: e.message
            })
        }
    }
}