const { createUser, updateUser, loginUser, changePassword } = require("../libs/users");
const log = require("../../config/logger");

module.exports = {
    createUser: async (req, res) => {
        try {
            const { body } = req;
            const user = await createUser(body);
            return res.status(201).json({
                status: "OK",
                message: "User created",
                user
            })
        } catch (e) {
            log.error(e.message);
            return res.status(e.status || 500).json({
                status: "Error",
                message: e.message,
            })
        }
    },
    updateUser: async (req, res) => {
        try {
            const { id } = req.params;
            const { body } = req;
            const user = await updateUser(id, body);
            return res.status(200).json({
                status: "OK",
                message: "User updated",
                user
            })
        } catch (e) {
            log.error(e.message);
            return res.status(e.status || 500).json({
                status: "Error",
                message: e.message
            })
        }
    },
    loginUser: async (req, res) => {
        try {
            const { body } = req;
            const user = await loginUser(body);

            return res.status(200).json({
                status: "OK",
                message: "User finded",
                user
            })

        } catch (e) {
            return res.status(e.status || 500).json({
                status: "Error",
                message: e.message
            })
        }
    },
    changePassword: async (req, res) => {
        try {
            const { id } = req.params;
            const { newPassword, beforePassword } = req.body;
            const user = await changePassword(id, newPassword, beforePassword);

            res.status(200).json({
                status: "OK",
                user
            })
        } catch (e) {
            res.status(e.status || 500).json({
                message: e.message
            })
        }
    }
}


