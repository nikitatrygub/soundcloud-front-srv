const { follow, unfollow } = require("../libs/followings");

module.exports = {
    follow: async (req, res) => {
        try {
            const { userId } = req.params;
            const { subscribeId } = req.body;
            const followTo = await follow(Number(userId), Number(subscribeId));
            res.status(200).json({
                status: "OK",
                followTo
            })
        } catch (e) {
            res.status(e.status || 500).json({
                message: e.message
            })
        }
    },
    unfollow: async (req, res) => {
        try {
            const { userId } = req.params;
            const { subscribeId } = req.body;
            const unfollowTo = await unfollow(Number(userId), Number(subscribeId));
            res.status(200).json({
                status: "OK",
                unfollowTo
            })
        } catch (e) {
            res.status(e.status || 500).json({
                message: e.message
            })
        }
    }
}