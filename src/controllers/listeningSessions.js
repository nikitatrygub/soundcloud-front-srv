const log = require("../../config/logger");
const { createSession } = require("../libs/listeningSessions");

module.exports = {
    createSession: async (req, res) => {
        try {
            const { userId, trackId } = req.body;
            const session = await createSession(userId, trackId);

            res.status(200).json({
                status: "OK",
                session
            })
        } catch (e) {
            res.status(e.status || 500).json({
                message: e.message
            })
        }
    }
}