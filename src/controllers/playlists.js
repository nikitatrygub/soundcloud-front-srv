const { createPlaylist, getUserPlaylists, addTrackToPlaylist } = require("../libs/playlists");
const log = require("../../config/logger");

module.exports = {
    createPlaylist: async (req, res) => {
        try {
            const fields = req.body;
            const { id } = req.params;
            const playlist = await createPlaylist(fields, id);

            res.status(200).json({
                playlist
            })
        } catch (e) {
            res.status(e.status || 500).json({
                message: e.message
            })
        }
    },
    getUserPlaylists: async (req, res) => {
        try {
            const { id } = req.params;
            const playlists = await getUserPlaylists(id);

            res.status(200).json({
                playlists
            })

        } catch (e) {
            res.status(e.status || 500).json({
                message: e.message
            })
        }
    },
    addTrackToPlaylist: async (req, res) => {
        try {
            const { trackId, playlistId } = req.params;
            const trackInPlaylist = await addTrackToPlaylist(trackId, playlistId);

            res.status(200).json({
                message: "OK",
                trackInPlaylist
            })
        } catch (e) {
            res.status(e.status || 500).json({
                message: e.message
            })
        }
    }
}