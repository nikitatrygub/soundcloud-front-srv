const { getGenres } = require("../libs/genres");
const log = require("../../config/logger");

module.exports = {
    getGenres: async (req, res) => {
        try {
            const genres = await getGenres();

            res.status(200).json(genres)
        } catch (e) {
            res.status(e.status || 500).json({
                message: e.message
            })
        }
    }
}