const webSocket = require("socket.io");
const { tracks } = require("../../config/sequelize");
const log = require("../../config/logger");


const connect = (server) => {
    let io = webSocket.listen(server);

    io.on('connection', async function (socket) {
        if (socket) log.info("Client is connected");


        updateLikes(socket, io);

        socket.on('disconnect', function (message) {
            log.info(message)
            io.emit('user disconnected');
        });

    });
}

const updateLikes = (socket, io) => {
    socket.on('trackStatus', async (trackId) => {
        await tracks.findByPk(trackId)
            .then(response => io.sockets.emit("trackStatus", { response, track: trackId }))
            .catch(e => io.sockets.emit("trackStatus", e.message))
    });
}

module.exports = {
    connect
}