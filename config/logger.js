const log4js = require("log4js");
const config = require("../config");
const log = log4js.getLogger();
log.level = config.logLevel;

module.exports = log