require("dotenv").config();
const config = require("../config");
const Sequalize = require("sequelize");
const log = require("./logger");

const { dbname, dbuser, dbpassword } = config;
const db = {};

const sequelize = new Sequalize(dbname, dbuser, dbpassword, {
    host: 'localhost',
    dialect: "mysql",
    operatorsAliases: false,
    pool: {
        max: 10000,
        min: 0,
        idle: 10000
    }
});


sequelize.authenticate()
    .then(() => log.info('Connection has been established successfully.'))
    .catch((err) => log.error('Unable to connect to the database:', err))


db.sequelize = sequelize;
db.users = sequelize.import("../src/models/users");
db.tracks = sequelize.import("../src/models/tracks");
db.genres = sequelize.import("../src/models/genres");
db.trackGenre = sequelize.import("../src/models/trackGenre");
db.playlists = sequelize.import("../src/models/playlists");
db.trackPlaylist = sequelize.import("../src/models/trackPlaylist");
db.trackLikes = sequelize.import("../src/models/trackLikes");
db.followings = sequelize.import("../src/models/followings");
db.listeningSessions = sequelize.import("../src/models/listeningSessions");

module.exports = db
